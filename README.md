# Setup

### NPM

First, we need to make sure your computer is setup to use npm. To do this open terminal either from your applications folder or from spotlight search by typing terminal. 

Once terminal is open type `npm -v`, if you get an output of some numbers such as `6.14.17`, please skip to install repo. If you do not, please install Node from https://nodejs.org/en/ and follow install instructions there. Once that is done, try to repeat this step.


### Download Files

Next, click the download button which is the next to the 'Clone' button. Feel to clone if you are comfortable with using 'git'. Once you have the files, we need to do open up that folder in terminal. Easiest way to do that is drag the folder to the terminal app in your dock. You can also open using file open or `cd` command to it. Once you have the name of this folder showing in terminal, proceed to the next step.

### Run Install

Next, you need to install packages required for this to run. From terminal inside project folder, run `npm install`

### Start Project

After that is completed, you are able to start your project. From terminal inside project folder, run `npm run start`

### Launch Browser

Your window should open to your project, but if not navigate to http://localhost:1234

# HTML Files

Add additional HTML files to the source directory and link them together from anchor links.

# SCSS or CSS Files

Be sure to include these using link files like the example or in the html `<link rel="stylesheet" href="scss/index.scss" />`

## Additional Resources

* https://sass-lang.com/ - scss documentation
* https://parceljs.org/ - what is pulling everything together
